const express = require('express');
const app = express();
const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');
const cors = require('cors');
const {google} = require('googleapis');
const bodyParser = require('body-parser');
var rp = require('request-promise');

app.use(cors());
app.use(bodyParser.json());

params = {
  resourceName: 'people/me',
  personFields: 'names'
}

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://hireme.auth0.com/.well-known/jwks.json`
  }),

  audience: 'https://api.forrest.com',
  issuer: `https://hireme.auth0.com/`,
  algorithms: ['RS256']
});

const checkScopes = jwtAuthz([ 'read:messages', 'read:users', 'read:user_idp_tokens']);

app.get('/api/private/order', checkJwt, checkScopes, function(req, res,next) {
    res.json({ message: "Congratulations, you've successfully ordered pizza via a private endpoint." });
  });


app.get('/api/hello', function(req,res){
    res.json({message:"Api Working"});
});

app.post('/api/private/connections', checkJwt, checkScopes,  function(req, res, next) {
  var options = { method: 'GET',
  url: 'https://hireme.auth0.com/api/v2/users/'+ req.body.userId,
  headers: { authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5VVXpNakEzUXpJeE5UVkRNVEpFUTBSQ05FSXdOMFk0UkRCRFFrVkRNamN6UVRNeE1rRXdOZyJ9.eyJpc3MiOiJodHRwczovL2hpcmVtZS5hdXRoMC5jb20vIiwic3ViIjoiQnh6MUczMlJ4b1RuMnhDOG44OTNBODNFc1gzRjNCamhAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vaGlyZW1lLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTMyMzI0Njg2LCJleHAiOjE1MzI0MTEwODYsImF6cCI6IkJ4ejFHMzJSeG9UbjJ4QzhuODkzQTgzRXNYM0YzQmpoIiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBjcmVhdGU6dXNlcl90aWNrZXRzIHJlYWQ6Y2xpZW50cyB1cGRhdGU6Y2xpZW50cyBkZWxldGU6Y2xpZW50cyBjcmVhdGU6Y2xpZW50cyByZWFkOmNsaWVudF9rZXlzIHVwZGF0ZTpjbGllbnRfa2V5cyBkZWxldGU6Y2xpZW50X2tleXMgY3JlYXRlOmNsaWVudF9rZXlzIHJlYWQ6Y29ubmVjdGlvbnMgdXBkYXRlOmNvbm5lY3Rpb25zIGRlbGV0ZTpjb25uZWN0aW9ucyBjcmVhdGU6Y29ubmVjdGlvbnMgcmVhZDpyZXNvdXJjZV9zZXJ2ZXJzIHVwZGF0ZTpyZXNvdXJjZV9zZXJ2ZXJzIGRlbGV0ZTpyZXNvdXJjZV9zZXJ2ZXJzIGNyZWF0ZTpyZXNvdXJjZV9zZXJ2ZXJzIHJlYWQ6ZGV2aWNlX2NyZWRlbnRpYWxzIHVwZGF0ZTpkZXZpY2VfY3JlZGVudGlhbHMgZGVsZXRlOmRldmljZV9jcmVkZW50aWFscyBjcmVhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIHJlYWQ6cnVsZXMgdXBkYXRlOnJ1bGVzIGRlbGV0ZTpydWxlcyBjcmVhdGU6cnVsZXMgcmVhZDpydWxlc19jb25maWdzIHVwZGF0ZTpydWxlc19jb25maWdzIGRlbGV0ZTpydWxlc19jb25maWdzIHJlYWQ6ZW1haWxfcHJvdmlkZXIgdXBkYXRlOmVtYWlsX3Byb3ZpZGVyIGRlbGV0ZTplbWFpbF9wcm92aWRlciBjcmVhdGU6ZW1haWxfcHJvdmlkZXIgYmxhY2tsaXN0OnRva2VucyByZWFkOnN0YXRzIHJlYWQ6dGVuYW50X3NldHRpbmdzIHVwZGF0ZTp0ZW5hbnRfc2V0dGluZ3MgcmVhZDpsb2dzIHJlYWQ6c2hpZWxkcyBjcmVhdGU6c2hpZWxkcyBkZWxldGU6c2hpZWxkcyB1cGRhdGU6dHJpZ2dlcnMgcmVhZDp0cmlnZ2VycyByZWFkOmdyYW50cyBkZWxldGU6Z3JhbnRzIHJlYWQ6Z3VhcmRpYW5fZmFjdG9ycyB1cGRhdGU6Z3VhcmRpYW5fZmFjdG9ycyByZWFkOmd1YXJkaWFuX2Vucm9sbG1lbnRzIGRlbGV0ZTpndWFyZGlhbl9lbnJvbGxtZW50cyBjcmVhdGU6Z3VhcmRpYW5fZW5yb2xsbWVudF90aWNrZXRzIHJlYWQ6dXNlcl9pZHBfdG9rZW5zIGNyZWF0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIGRlbGV0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIHJlYWQ6Y3VzdG9tX2RvbWFpbnMgZGVsZXRlOmN1c3RvbV9kb21haW5zIGNyZWF0ZTpjdXN0b21fZG9tYWlucyByZWFkOmVtYWlsX3RlbXBsYXRlcyBjcmVhdGU6ZW1haWxfdGVtcGxhdGVzIHVwZGF0ZTplbWFpbF90ZW1wbGF0ZXMiLCJndHkiOiJjbGllbnQtY3JlZGVudGlhbHMifQ.hF2wnWGJTlVOOWoxXjNu2NQdoXXWi2Pm9RgddVeu9dKEAxb6AZPMwI_9G4NGCxvYpyte7o2kU1nPahosVDqibFxUtFA6I09y0bnK_ec2sMQZZlWYsnUZdMMTmb-EEf0bu9PzB0iGPrZiuEXBIDcAkVS59wdn5txCVgbforZoDICAn5CU_2Wx6BCTu4z54pdyvhbSaBjRYBrki-hGqv--o3_oYj402IYtKl3J61tFNt-fwd5vqoZm4umxbthL4pVireHzputCFCl0dfqgqhsa8GX2kCBhC5k2ed-tZcMW3WWIDBWUczdMctE_yePm99eNkT-m0ddjLfHPo_hfEaiuUg',
  'Content-Type':'application/json' } };

  rp(options)
      .then(function(body){
        var json = JSON.parse(body);
        res.json(getConnections(json.identities[0].accessToken));
      })
      .catch(function(err){
        console.log(err);
      });

  });

  function getConnections(accessToken) {
      console.log(accessToken);
      
    const people = google.people({
      version:'v1',
      auth: accessToken
    });
    console.log('getting connections...');
   return people.people.connections.list(params);
  };

app.listen(4500);
console.log('Server listening on http://localhost:4500. The Angular app will be built and served at http://localhost:4200. Hit /api/hello to test');