FROM nginx:latest
LABEL maintainer="forrestknightjr@gmail.com"
COPY ./dist/auth0 /usr/share/nginx/html
