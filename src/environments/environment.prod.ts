export const environment = {
  production: true,
  API_URL: 'http://forrestknight.io:4500/api',
  redirectUri: 'http://forrestknight.io/#/callback'
};
