import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/login.service';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  message: string;
  constructor(public auth: AuthService , public orderService: OrderService) { }

  ngOnInit() {
  }

  order() {
    this.orderService.order()
      .subscribe(
        data => this.message = data.message,
        error => this.message = error
      );
  }
}
