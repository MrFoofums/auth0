import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '../../../node_modules/@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { environment } from '../../environments/environment';

interface IApiResponse {
  message: string;
}

interface IConnectionsReponse {
  response: any;
}

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  API_URL = environment.API_URL;

  constructor(public http: HttpClient) {}

  public order(): Observable<IApiResponse> {
    return this.http
      .get<IApiResponse>(`${this.API_URL}/private/order`, {
        headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('access_token')}`).set('Contet-Type', ``)
      });
  }

  public getConnections(userId: any): Observable<IConnectionsReponse> {
    const tokenHeader  = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('access_token')}`);
    return this.http
    .post<IConnectionsReponse>(`${this.API_URL}/private/connections`, {userId: userId}, {
      headers: tokenHeader
    });
  }
}
