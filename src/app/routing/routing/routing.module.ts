import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { HomeComponent } from '../../home/home.component';
import { CallbackComponent } from '../../callback/callback.component';
import { ProfileComponent } from '../../profile/profile.component';

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent,
},
{path: 'callback', component: CallbackComponent},
{path: 'profile', component: ProfileComponent},
{path: '', component: HomeComponent},

];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true
      }
    )
  ],
exports: [
  RouterModule
]
})
export class RoutingModule { }
