import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/login.service';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile: any;
  connectionsList: any;
  constructor(public auth: AuthService, private orderService: OrderService) { }

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }

  connections() {
    this.orderService.getConnections(this.profile.sub).subscribe(
      data => this.connectionsList = data.response,
      error => this.connectionsList = error
    );
  }

}
