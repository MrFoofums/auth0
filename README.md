# Auth0

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

1. The API utilizes the example provided by Auth0, running on port 4500
2. Per the FAQ, and to save on time, only the minimal code written to satisfy the requirements exists.
3. Despite having Google's access token, I wasn't able to retrieve the users connections. The documentation (on Google's part) isn't clear to me on how to `properly` use an already acquired token when making request. I am using their client library, I'm interested in your approach.
4. Looking forward to further discussion.

# Running Locally
1. Make sure all dependencies are installed
2. cd into /api and do `npm start`
3. In the root folder do `ng serve` and navigate to `http://localhost:4200/`


### Resources
* https://github.com/auth0-samples

### Requirements
1. Show how a new customer can sign up and an existing customer can sign in with
email/password or Google
2. Ensure that if a customer signs in with either an email/password or Google, it will be
treated as the same user if they use the same email address
3. Show that the solution can be built as a “modern” web application (SPA) which can then
securely call the API backend of Pizza 42 using OAuth.
4. Require that a customer has a verified email address before they can place a pizza
order, but they should still be able to sign into the app.
5. Use Auth0 features to gather additional information about a user (specifically their
gender) without prompting them directly.
6. Use Auth0 to call the Google People API to fetch the total number of Google
connections a user has and store that count in their user profile.



## Dev Stuff
* docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro --net=nginx-proxy jwilder/nginx-proxy:latest